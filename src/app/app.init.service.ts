import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SmartConfig, SmartConfigOptions } from '@consultingwerk/smartcomponent-library';

@Injectable()
export class AppInitService {

    config: SmartConfigOptions;

    constructor(private http: HttpClient) {

    }

    init() {
        console.log(this.http);
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                this.config = {
                    serviceURI: 'http://localhost:8820'
                   }
                   resolve();
            }, 6000);
           
        });
    }
}