import {
  SmartRouteGuard,
  SmartComponentLibraryModule,
  SmartFormComponent,
  CustomSmartForm,
  DataSourceRegistry,
  SmartViewerRegistryService,
  SmartTabFolderRegistryService,
  SmartViewManagerService,
  SmartFormInstanceService,
  SmartToolbarRegistry,
  SmartFilterRegistry,
  SmartDialogService,
  DialogButtons,
} from '@consultingwerk/smartcomponent-library';
import {
  Component,
  Injector,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  NgModule,
  ModuleWithProviders,
  ViewChild,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { LauncherModalComponent } from './launcher-modal/launcher-modal.component';
import { environment } from '../../../environments/environment';
@CustomSmartForm('dynamicLauncher')
@Component({
  selector: 'dynamic-launcher-form',
  templateUrl: './dynamic-launcher.form.html',
  styleUrls: ['./dynamic-launcher.form.css'],
  viewProviders: [
    DataSourceRegistry,
    SmartViewManagerService,
    SmartFormInstanceService,
    SmartToolbarRegistry,
    SmartViewerRegistryService,
    SmartTabFolderRegistryService,
    SmartFilterRegistry,
  ],
})
export class DynamicLauncherFormComponent extends SmartFormComponent
  implements OnInit, OnDestroy, OnChanges, AfterViewInit {
  @ViewChild('dynamicLauncherForm')
  dynamicLauncherForm: SmartFormComponent;

  objectMasterLookupGridLayout = `frontend://${environment.baseHref}assets/object-master-grid.layout.json`
  selectedLayout: string;
  selectedObjectMaster: string;

  constructor(
    injector: Injector,
    private router: Router
  ) {
    super(injector);
  }

  ngOnInit() {
    // Add your own initialization logic here

    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
  }

  ngAfterViewInit() {
    console.log('after view init')
    const formName: string = this.route.snapshot.params.formName;
    console.log(this.route.snapshot.params)
    if (formName && formName !== '') {
      this.selectedLayout = `/SmartForm/Form/${formName}`;
      console.log(this.selectedLayout)
    }
  }

  launchForm(objectMasterName: string) {
    this.router.navigate([
      {
        outlets: {
          view: `dynamic-launcher/${objectMasterName}`,
        },
      },
    ]);
  }

  @HostListener('document:keypress', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if (event.key.toLowerCase() !== 'enter') {
      return;
    }
    if (this.selectedObjectMaster && this.selectedObjectMaster !== '') {
      this.launchForm(this.selectedObjectMaster);
    }
  }

}

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'dynamic-launcher',
        component: DynamicLauncherFormComponent,
        canActivate: [SmartRouteGuard],
        outlet: 'view',
        data: {
          BreadcrumbLabelTemplate: 'Dynamic Launcher',
          BrowserTitleTemplate: 'Dynamic Launcher',
          FormId: 'dynamicLauncher',
        },
      },
      {
        path: 'dynamic-launcher/:formName',
        component: DynamicLauncherFormComponent,
        canActivate: [SmartRouteGuard],
        outlet: 'view',
        data: {
          BreadcrumbLabelTemplate: 'Dynamic Launcher',
          BrowserTitleTemplate: 'Dynamic Launcher',
          FormId: 'dynamicLauncher',
        },
      },
    ]),
    SmartComponentLibraryModule,
  ],
  declarations: [DynamicLauncherFormComponent, LauncherModalComponent],
  entryComponents: [DynamicLauncherFormComponent, LauncherModalComponent],
  exports: [RouterModule],
})
export class DynamicLauncherFormModule {
  static entryComponents = [
    DynamicLauncherFormComponent,
    LauncherModalComponent,
  ];

  static forRoot(): ModuleWithProviders<DynamicLauncherFormModule> {
    return {
      ngModule: DynamicLauncherFormModule,
    };
  }
}
