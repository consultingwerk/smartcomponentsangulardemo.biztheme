import { 
    SmartRouteGuard,
    SmartComponentLibraryModule,
    SmartFormComponent,
    CustomSmartForm,
    DataSourceRegistry,
    SmartViewerRegistryService,
    SmartTabFolderRegistryService,
    SmartViewManagerService,
    SmartFormInstanceService,
    SmartToolbarRegistry,
    SmartFilterRegistry
} from '@consultingwerk/smartcomponent-library';
import { Component, Injector, OnInit, OnDestroy, OnChanges, SimpleChanges, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { environment } from '../../../environments/environment';
@CustomSmartForm('cce')
@Component({
    selector: 'cce-form',
    templateUrl: '../../../../node_modules/@consultingwerk/smartcomponent-library/ui/form/smart-form.component.html',
    viewProviders: [
        DataSourceRegistry,
        SmartViewManagerService,
        SmartFormInstanceService,
        SmartToolbarRegistry,
        SmartViewerRegistryService,
        SmartTabFolderRegistryService,
        SmartFilterRegistry
    ]
})
export class CceFormComponent extends SmartFormComponent implements OnInit, OnDestroy, OnChanges {

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        // Add your own initialization logic here
        
        this.setFormConfiguration(`frontend://${environment.baseHref}assets/cce.layout.json`);
        
        super.ngOnInit();
        alert('CCE');
    }

    ngOnDestroy() {

        super.ngOnDestroy();
    }

    ngOnChanges(changes: SimpleChanges) {

        super.ngOnChanges(changes);
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([{
            path: 'cce',
            component: CceFormComponent,
            canActivate: [SmartRouteGuard],
            outlet: 'view',
            data: {
                
                BrowserTitleTemplate: 'CCE Demo', 
                FormId: 'cce'
            }
        }]), 
        SmartComponentLibraryModule
    ],
    declarations: [
        CceFormComponent
    ],
    entryComponents: [
        CceFormComponent
    ],
    exports: [
        RouterModule
    ]
})
export class CceFormModule { }