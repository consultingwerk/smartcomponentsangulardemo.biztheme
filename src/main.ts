import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { SmartConfig } from '@consultingwerk/smartcomponent-library';

if (environment.production) {
  enableProdMode();
}

export class MySmartConfig {
  serviceURI: 'http://invalid.com';
  defaultRoute: 'haha'
}
platformBrowserDynamic().bootstrapModule(AppModule, {
  providers: [
    { provide: SmartConfig, useClass: MySmartConfig, deps: [] }
  ]
})
  .catch(err => console.log(err));
